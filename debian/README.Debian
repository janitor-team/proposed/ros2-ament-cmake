The ament build system in Debian has been slightly tweaked to accommodate
Debian policy requirements:

  * Prebuilt ROS 2 packages for Debian are installed in /usr, not /opt/ros.

  * The AMENT_PREFIX_PATH will always implicitly include /usr to make
    prebuilt ROS 2 packages work without additional setup. Users can disable
    this behavior by setting the environment variable
    AMENT_PREFIX_NO_IMPLICIT_USR=1

  * The ament_package() macro gained an undocumented CMAKE_DIR option to
    override the install location for CMake config files. This is intended for
    internal use by the Debian Robotics Team *ONLY*. (For the curious: the
    Multi-Arch specification requires architecture-dependent files, including
    CMake config files, to be installed in /usr/lib/<triplet>, not /usr/share).

 -- Timo Röhling <roehling@debian.org>  Fri, 06 May 2022 15:09:05 +0200
