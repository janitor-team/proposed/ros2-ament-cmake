cmake_minimum_required(VERSION 3.20)
project(oldbaz VERSION 1.0.0 LANGUAGES CXX)

find_package(ament_cmake REQUIRED)
find_package(oldbarlib REQUIRED)

add_executable(${PROJECT_NAME} src/source.cpp)
install(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION bin
)
ament_target_dependencies(${PROJECT_NAME} oldbarlib)

ament_package()

